//--------------->>>>>    working code


// define([
//     'jquery'
//
// ], function ($) {
//
//
//     $.widget('elogic.inputLengthValChecker', {
//         options: {
//             'wordLength': '4'
//         },
//
//         _create: function () {
//             let self = this.element;
//             let _self = this
//             debugger;
//             self.on('change keyup past', function () {
//                 let button = self.siblings('button');
//                 let value = self.val();
//                 if (value.length >= _self.options.wordLength) {
//                     button.prop('disabled', true)
//                 }else{
//                     button.prop('disabled', false)
//                 }
//             })
//         },
//
//     });
//
//     return $.elogic.inputLengthValChecker;
// });





//------------------>>> second working widget slightly modified

// define([
//     'jquery'
//
// ], function ($) {
//
//
//     $.widget('elogic.inputChecker', {
//         options: {
//             'wordLength': '',
//             'buttonClass' : ''
//         },
//
//         _create: function (){
//             this._bind();
//         },
//
//         _bind:function (){
//             this._on({
//                 change:'_buttonEnabler',
//
//             })
//         },
//
//         _buttonEnabler:function (){
//             let self = this;
//             let selfElem = this.element;
//             let value = selfElem.val();
//             let chekingObjectLenght = selfElem.val().length;
//             let givenParameter = this.options.wordLength;
//             let button = '.' + self.options.buttonClass;
//             debugger;
//             if(chekingObjectLenght >= givenParameter){
//                 console.log('start code');
//                 $(button).prop('disabled',false);
//             }else{
//                 $(button).prop('disabled',true);
//             }
//         }
//
//     });
//
//     return $.elogic.inputChecker;
// });







define([
    'jquery'

], function ($) {


    $.widget('elogic.inputChecker', {
        options: {
            'wordLength': '',
            'buttonClass' : '',
            'textChangerPos':'',
            'textChangerNeg':''
        },

        _create: function (){
            this._bind();
        },

        _bind:function (){
            this._on({
                change:'_buttonEnabler',

            })
        },

        _buttonEnabler:function (){
            let self = this;
            let selfElem = this.element;
            let value = selfElem.val();
            let chekingObjectLenght = selfElem.val().length;
            let givenParameter = this.options.wordLength;
            let textParametrPos = this.options.textChangerPos;
            let textParametrNeg = this.options.textChangerNeg;
            let button = '.' + self.options.buttonClass;
            debugger;
            if(chekingObjectLenght >= givenParameter){
                $(button).text(textParametrPos);
            }else{
                $(button).text(textParametrNeg);
            }
        }

    });

    return $.elogic.inputChecker;
});
